# OTUS.ru Fullstack разработчик JavaScript

## Prerequisites

* [Node.js](https://nodejs.org/en/download/) JavaScript runtime built on Chrome's V8 JavaScript engine.

* [IntelliJ IDEA](https://www.jetbrains.com/idea/) or [WebStorm](https://www.jetbrains.com/webstorm/) 
  is used by most of us, we recommend it, although it's not required. It's up to you what version
  (Community or Ultimate) to choose.
  
* [Git](https://git-scm.com/) Distributed version control system designed to handle everything
