function sum(a) {
    let sum = a;
    if (a === undefined) {
        throw new Error('Invalid argument');
    }
    const f = function (a) {
        if (a === undefined) {
            return sum;
        }
        sum += a;
        return f;
    };
    return f;
}

module.exports = sum;