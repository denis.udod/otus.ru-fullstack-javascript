### Введение в курс Modern JavaScript Frameworks. Домашнее задание

#### Написать функцию суммирования значений
Написать функцию sum, которая может быть исполнена любое количество раз с не `undefined` аргументом.
Если она исполнена без аргументов, то возвращает значение суммы всех переданных до этого значений.
```$javascript
sum(1)(2)(3)....(n)() === 1 + 2 + 3 + ... + n
```
Критерии оценки: 
 
1. Факт свершения действия ДЗ отправлена на проверку 
    * +1 балл
2. Степень выполнения ДЗ*
    * +1 балл – результат скорее не достигнут, чем достигнут
    * +2 баллов – результат скорее достигнут, чем не достигнут
    * +3 балла – результат достигнут полностью
3. Способ выполнения ДЗ
    * +1 балл – использован неоптимальный способ достижения результата
    * +2 балл – оптимальный способ достижения результата использован частично
    * +3 балл -- использован наиболее оптимальный способ достижения результата 
4. Скорость выполнения ДЗ
    * +1 балл - работа сдана под конец курса
    * +2 балла - работа сдана с отставанием
    * +3 балла - дедлайн соблюден
    
#### Solution
The `sum` function inside contains the` sum` variable.
The variable `sum` is responsible for storing the commutative value of the arguments.
The sum function initializes the variable sum from the passed arguments and returns a new function f.
If there is no argument, the `sum` function throws an error.
The `f` function is responsible for increasing the variable by the argument passed, or, in the case of the argument` undefindd`, returns the result.

see [sum.js](sum.js)
```$javascript
function sum(a) {
    let sum = a;
    if (a === undefined) {
        throw new Error('Invalid argument');
    }
    const f = function (a) {
        if (a === undefined) {
            return sum;
        }
        sum += a;
        return f;
    };
    return f;
}
```

##### How to test:
1. Add the line `module.exports = sum;` to the end of `sum.js`
    ```$javascript
    function sum(a) {
        ...
    }
    module.exports = sum;
    ```
2. Create new `test.js` file
    ```$javascript
    const sun = require('./sum');
    console.log(sum(1)(2)(3)());
    ```
3. Execute `test.js` using `Node.js`(the `node` must be in the path) 
    ```$bash
    $ node test.js
    ``` 
4. In the console you should see the result 6
```
$ node test.js
6
```